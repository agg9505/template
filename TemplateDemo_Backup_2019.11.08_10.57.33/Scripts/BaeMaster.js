﻿//marcar menu
//var url = 'http://apps.bae.gym/BaseDatos/PlanProduccion/';
var url = window.location.pathname;
$('li .kt-menu__item  a[href="' + url + '"]').parents("li").addClass('kt-menu__item--here');
var first = $(location).attr('pathname');
first.toLowerCase();
first = first.split("/")[1];
$("#MenuNavBarHere li:contains('" + first + "')").addClass('kt-menu__item--here');
//cambiar lenguaje
function SetLanguage(l) {
    $.ajax({
        type: "POST",
        cache: false,
        data: { len: l },
        url: '/BAE/SetLenguage', beforeSend: function () {
        }
    }).done(function (data) {
        location.reload();
    }).fail(function (data) {
        location.reload();
    })
};
//Mostrar respuesta
function GetAnswerNShowIT(a) {
    for (i = 0; i < a.Messages.length; i++) {
        switch (a.Type[i]) {
            case 1:
                ShowNotification(a.Messages[i], 'success');
                break;
            case 2:
                ShowNotification(a.Messages[i], 'warning');
                break;
            case 3:
                ShowNotification(a.Messages[i], 'danger');
                break;
            case 4:
                ShowNotification(a.Messages[i], 'info');
                break;
            case 5:
                ShowNotification(a.Messages[i], 'brand');
                break;
            default:
                break;
        }
    }
    setTimeout(() => {
        NotificationSearchStart();
    }, 2000);
}
//Agregar notificacion post a api
function ShowNotification(content, type) {
    var notify = $.notify(content, {
        type: type,
        allow_dismiss: 'false',
        newest_on_top: 'true',
        mouse_over: 'true',
        spacing: 10,
        timer: 2000,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        offset: {
            x: 30,
            y: 30
        },
        delay: 1000,
        z_index: 10000,
        animate: {
            enter: 'animated slideInRight',
            exit: 'animated slideOutRight'
        }
    });
}
//traer partialview y convertirla a texto
function GetPartialViewHTML(rute) {
    $.ajax({
        type: "GET",
        cache: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        },
        url: rute, beforeSend: function () {
        }
    }).done(function (data) {
        return JSON.stringify(data);
    }).fail(function (data) {
        return 'Fail Get PartialView';
    });
}
//traer partialview e insertarla en target
function GetPartialView(rute, target) {

    $.ajax({
        type: "GET",
        cache: false,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        },
        url: rute, beforeSend: function () {
        }
    }).done(function (data) {
        $('#' + target).html(data).ready().fadeIn("slow");
    }).fail(function (data) {
        alert('Fail Get PartialView');
    });
}
function CleanForm(e, action) {
    e.attr('action', action);
    e.find(':input').each(function () {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}
