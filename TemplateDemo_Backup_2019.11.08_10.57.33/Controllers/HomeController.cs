﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAEClassLibrary;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;
using TemplateDemo.Models;


namespace TemplateDemo.Controllers
{
    public class HomeController : BAEController
    {
        BAETDDB db = new BAETDDB();
        public ActionResult Index()
        {
           
            SetCurrentDepartmentMenu("OpEx");
            return View();
        }

        public ActionResult About()
        {
          
            return View();
        }
        public ActionResult Deparment(string dep)
        {
            if (dep != null)
            {
                SetCurrentDepartmentMenu(dep);
            }
            return View("Index");
        }

        public ActionResult Contact()
        {
            AnswerResult ar = new AnswerResult();
            ViewBag.AnswerResult = JsonConvert.SerializeObject(ar);

            return View();
        }
        public ActionResult Logout()
        {
            Session.RemoveAll();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            return Redirect("https://apps.bae.gym/");
        }

    }
}