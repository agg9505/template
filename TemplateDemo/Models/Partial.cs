﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TemplateDemo.Models;

namespace TemplateDemo.Models
{
    
    public partial class AppMenuViewModel
    {
        public string App { get; set; }
    }
    public partial class EmployeeViewModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
    public partial class ModuleViewModel
    {
        public int ModuleID { get; set; }
        public string ModuleName { get; set; }
    }
    public class PermissionEmployeeViewModel
    {
        public int PermissionID { get; set; }
        public int EmployeeID { get; set; }
        public ModuleViewModel Module
        {
            get;
            set;
        }
        [UIHint("_ModelDropPartialView")]
        public int ModuleID { get; set; }
        public bool AccessP { get; set; }
        public bool CreateP { get; set; }
        public bool ReadP { get; set; }
        public bool UpdateP { get; set; }
        public bool DeleteP { get; set; }
        public bool PrintP { get; set; }
    }
    public class PermissionProfileViewModel
    {
        public int PermissionID { get; set; }
        public int ProfileID { get; set; }
        public ModuleViewModel Module
        {
            get;
            set;
        }
        [UIHint("_ModelDropProfilePartialView")]
        public int ModuleID { get; set; }
        public bool AccessP { get; set; }
        public bool CreateP { get; set; }
        public bool ReadP { get; set; }
        public bool UpdateP { get; set; }
        public bool DeleteP { get; set; }
        public bool PrintP { get; set; }
    }
    [MetadataType(typeof(Menu))]
    public partial class Menu
    {
        public List<Menu> Hijos
        {
            get
            {
                BAETDDB db = new BAETDDB();
                List<Menu> hijitos = db.Menu.Where(a => a.DadMenu == MenuID).OrderBy(a=>a.SubMenuOrder).ToList();
                return hijitos.Count() > 0 ? hijitos : null;
            }
            set
            {
            }
        }
    } 
   
    public class PermissionViewModel
    {
        public int Id { get; set; }
        public string Permission { get; set; }
    }
    public class KPIViewModel
    {
        public int KPIID { get; set; }
        public string AppName { get; set; }
        public string KPIName { get; set; }
        public string KPIDescription { get; set; }
        public bool Active { get; set; }
        public string Url { get; set; }
    }
    public class predashboard
    {
        public int DashboardID { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int h { get; set; }
        public int w { get; set; }

    }
    public class KPIListViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string App { get; set; }
        public string Desc { get; set; }

    }
    public partial class NotificationsEmployeeDestinationViewModel
    {
        public int NotificationsEmployeeDestinationID { get; set; }
        public int NotificationListID { get; set; }
        public EmployeeViewModel EmployeeVM
        {
            get;
            set;
        }
        [UIHint("_EmployeeDropPartialView")]
        public int EmployeeID { get; set; }
    }

}