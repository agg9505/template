//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TemplateDemo.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employees
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employees()
        {
            this.Employees1 = new HashSet<Employees>();
            this.EmployeeProfile = new HashSet<EmployeeProfile>();
            this.EmployeeModulePermission = new HashSet<EmployeeModulePermission>();
            this.NotificationsEmployeeDestination = new HashSet<NotificationsEmployeeDestination>();
        }
    
        public int EmployeeID { get; set; }
        public string EmployeeUserName { get; set; }
        public string EmployeePassword { get; set; }
        public Nullable<int> JobTitleID { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeStampNumber { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSecondName { get; set; }
        public string EmployeeFatherLastName { get; set; }
        public string EmployeeMotherLastName { get; set; }
        public string EmployeeFullName { get; set; }
        public string EmployeeNameViewChar { get; set; }
        public int EmployeeShift { get; set; }
        public string EmployeeType { get; set; }
        public string EmployeeEmail { get; set; }
        public string EmployeeMobilePhone { get; set; }
        public string EmployeeExtension { get; set; }
        public string EmployeeProfileImage { get; set; }
        public string EmployeeLocation { get; set; }
        public Nullable<int> EmployeeReportsTo { get; set; }
        public string EmployeeAssignedDeparments { get; set; }
        public Nullable<bool> EmployeeStatus { get; set; }
        public System.DateTime EmployeeStartDate { get; set; }
        public Nullable<System.DateTime> EmployeeEndDate { get; set; }
        public Nullable<bool> EmployeeChar { get; set; }
        public Nullable<int> ClassID { get; set; }
        public string EmployeeTimeCode { get; set; }
        public Nullable<int> ContractTypeID { get; set; }
        public Nullable<System.DateTime> EmployeeBirthDate { get; set; }
        public string EmployeeGender { get; set; }
        public string EmployeeBloodType { get; set; }
        public string EmployeeScholarship { get; set; }
        public Nullable<int> EmployeeChildrenNumber { get; set; }
        public string EmployeeMaritalStatus { get; set; }
        public string EmployeeRotationType { get; set; }
        public string EmployeeReason { get; set; }
        public Nullable<int> AreaID { get; set; }
        public string EmployeeIPTLead { get; set; }
        public Nullable<bool> EmployeeAvailableHrs { get; set; }
        public Nullable<bool> EmployeeAuthorizeDoubleHrs { get; set; }
        public Nullable<bool> EmployeeAuthorizeTripleHrs { get; set; }
        public Nullable<int> LineID { get; set; }
        public string EmployeePersonalPhone { get; set; }
        public Nullable<int> EmployeeEconomicDependents { get; set; }
        public Nullable<int> EmployeeTypeID { get; set; }
        public Nullable<int> EmployeeBuildingID { get; set; }
        public Nullable<bool> EmployeeStatusTraining { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employees> Employees1 { get; set; }
        public virtual Employees Employees2 { get; set; }
        public virtual Employees Employees11 { get; set; }
        public virtual Employees Employees3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeProfile> EmployeeProfile { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeModulePermission> EmployeeModulePermission { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NotificationsEmployeeDestination> NotificationsEmployeeDestination { get; set; }
        public virtual JobTitles JobTitles { get; set; }
    }
}
