﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TemplateDemo.Models;
using BAEClassLibrary;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace TemplateDemo.Controllers
{
    public class NotificationManagerController : BAEController
    {
        BAETDDB db = new BAETDDB();
        // GET: NotificationManager
        public ActionResult Index(int? AppID)
        {
            AnswerResult ar = (AnswerResult)TempData["AnswerResult"] ?? new AnswerResult(); TempData["AnswerResult"] = null;
            ViewBag.AnswerResult = JsonConvert.SerializeObject(ar);
            TemplateDemo.Models.Apps app = db.Apps.FirstOrDefault(a => a.AppID == AppID);
            if (app==null)
            {
                app = new TemplateDemo.Models.Apps();
            }
            return View(app);
        }
        public ActionResult NotificationCreation()
        {
            
            return View();
        }
        public JsonResult GetAppDrop(int? AppID)
        {

            if (AppID != null && AppID > 0)
            {

                var products = db.Apps.Where(p => p.AppID==AppID).Select(x => new EmployeeViewModel
                {
                    Value = x.AppID,
                    Text = x.AppName
                });
                return Json(products, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var products = db.Apps.Select(x => new EmployeeViewModel
                {
                    Value = x.AppID,
                    Text = x.AppName
                });
                return Json(products, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ReadNotificationList([DataSourceRequest] DataSourceRequest request, int? AppID)
        {
        
            if (AppID!=null&&AppID>0)
            {
                var list = db.NotificationList.Where(a=>a.AppID==AppID).Select(s => new
                {
                    NotificationListID = s.NotificationListID,
                    Apps = new { AppID = s.AppID, AppName = s.Apps.AppName },
                    Subject = s.Subject,
                    Message = s.Message,
                    Active = s.Active,
                    NotificationType = s.NotificationType,
                    AlertType = s.AlertType,
                    Icon = s.Icon,
                    Link = s.Link,
                }).ToList();
                return Json(list.ToDataSourceResult(request));
            }
            else
            {
                var list = db.NotificationList.Select(s => new
                {
                    NotificationListID = s.NotificationListID,
                    Apps = new { AppID = s.AppID, AppName = s.Apps.AppName },
                    Subject = s.Subject,
                    Message = s.Message,
                    Active = s.Active,
                    NotificationType = s.NotificationType,
                    AlertType = s.AlertType,
                    Icon = s.Icon,
                    Link = s.Link,
                }).ToList();
                return Json(list.ToDataSourceResult(request));
            }
        
        }
        public ActionResult Detail(int? id)
        {
            AnswerResult ar = (AnswerResult)TempData["AnswerResult"] ?? new AnswerResult(); TempData["AnswerResult"] = null;
            NotificationList nl = id != null && id != 0 ? db.NotificationList.FirstOrDefault(a => a.NotificationListID == id) : new NotificationList() { Active = false };
            ViewBag.Action = nl.NotificationListID != 0 ? "Edit" : "Create";
            ViewBag.AppID = db.Apps.Select(c => new SelectListItem
            {
                Value = c.AppID.ToString(),
                Text = c.AppName.ToString()
            }).OrderBy(a => a.Text);
            ViewBag.AnswareResult = JsonConvert.SerializeObject(ar);
            return View(nl);
        }
        public ActionResult Create(NotificationList nl)
        {
            AnswerResult ar = new AnswerResult();
            if (ModelState.IsValid)
            {
                try
                {
                    db.NotificationList.Add(nl);
                    db.SaveChanges();
                    ar.Type.Add(1);
                    ar.Messages.Add(Resources.AnswerResultSuccess);
                }
                catch (DbEntityValidationException E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    foreach (var eve in E.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ar.Type.Add(3);
                            ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                        }
                    }
                }
                catch (Exception E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    if (E.InnerException != null)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                    else
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }

                }
            }
            return Json(ar);
        }
        public ActionResult Send(string Subject, string Message, int NotificationType, int AlertType, string Link, IEnumerable<int> EmployeesL, IEnumerable<int> DepartmentsL, IEnumerable<int> JobTitlesL, bool All)
        {
            AnswerResult ar = new AnswerResult();
       
                try
                {
                List<int> receivers = new List<int>();
                if (All)
                {
                    receivers.AddRange(db.Employees.Where(a => a.EmployeeStatus.Value==true).Select(a => a.EmployeeID).ToList());
                }
                else
                {
                    if (EmployeesL != null)
                    {
                        receivers.AddRange(EmployeesL);
                    }
                    if (JobTitlesL != null)
                    {
                        receivers.AddRange(db.Employees.Where(a => JobTitlesL.Contains(a.JobTitleID.Value)).Select(a => a.EmployeeID).ToList());
                    }
                    if (DepartmentsL != null)
                    {
                        receivers.AddRange(db.Employees.Where(a => DepartmentsL.Contains(a.JobTitles.DepartmentID)).Select(a => a.EmployeeID).ToList());
                    }
                }
                
                Models.Notifications n = null;
                 DateTime now = DateTime.Now;
                foreach (var item in receivers)
                {
                    n = new Models.Notifications()
                    {
                        NotificationListID =1,
                        CreationDate = now,
                        EmployeeCreatorID = UserContext.current.EmployeeID,
                        Icon = "fa fa-bell",
                        IntendedEmployeeID = item,
                        Link = string.IsNullOrEmpty(Link)?"#":Link,
                        Message = Message,
                        Seen = false,
                        SeenDate = null,
                        Subject = Subject,
                        NotificationType =NotificationType,
                        AlertType = AlertType
                    };
                    db.Notifications.Add(n);
                    db.SaveChanges();
                }
            
                    ar.Type.Add(1);
                    ar.Messages.Add(Resources.AnswerResultSuccess);
                }
                catch (DbEntityValidationException E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    foreach (var eve in E.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ar.Type.Add(3);
                            ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                        }
                    }
                }
                catch (Exception E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    if (E.InnerException != null)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                    else
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }

                }
            
            return Json(ar);
        }
        [HttpPost]
        public ActionResult Edit(NotificationList nl)
        {
            AnswerResult ar = new AnswerResult();
            if (ModelState.IsValid)
            {
                try
                {
                    if (!UserContext.current.IsAdmin)
                    {
                        var val = nl.Active;
                        nl = db.NotificationList.FirstOrDefault(a => a.NotificationListID == nl.NotificationListID);
                        nl.Active = val;
                    }
                    db.NotificationList.Attach(nl);
                    db.Entry(nl).State = EntityState.Modified;
                    db.SaveChanges();
                    ar.Type.Add(1);
                    ar.ID = nl.NotificationListID;
                    ar.Messages.Add(Resources.AnswerResultSuccess);
                }
                catch (DbEntityValidationException E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    foreach (var eve in E.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ar.Type.Add(3);
                            ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                        }
                    }
                }
                catch (Exception E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    if (E.InnerException != null)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                    else
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }

                }
            }

            return Json(ar);
        }
        public ActionResult ShowDeleteConfirmation(int id)
        {
            NotificationList nl = db.NotificationList.FirstOrDefault(a => a.NotificationListID == id);
            //verificamos que no tenga dependencias 
             switch (nl.NotificationType)
            {
                case 1:
                    ViewBag.NotificationType =  Resources.Notification;
                    break;
                case 2:
                    ViewBag.NotificationType = Resources.Event;
                    break;
                case 3:
                    ViewBag.NotificationType = Resources.Log;
            break;
            };
            switch (nl.AlertType)
            {
                case 1:
                    ViewBag.AlertType = Resources.Success;
                    break;
                case 2:
                    ViewBag.AlertType = Resources.Warning;
                    break;
                case 3:
                    ViewBag.AlertType = Resources.Danger;
                    break;
                case 4:
                    ViewBag.AlertType = Resources.Informative;
                    break;
                case 5:
                    ViewBag.AlertType = Resources.Brand;
                    break;
            };
            ViewBag.EntryFound = false;
            if (nl.NotificationsEmployeeDestination.Count > 0)
            {
                ViewBag.EntryFound = true;
            }
            return PartialView(nl);
        }
        [HttpPost]
        public ActionResult Delete(NotificationList nl)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                var ned = db.NotificationsEmployeeDestination.Where(a => a.NotificationListID == nl.NotificationListID).ToList();
                db.NotificationsEmployeeDestination.RemoveRange(ned);
                NotificationList l = db.NotificationList.FirstOrDefault(a => a.NotificationListID == nl.NotificationListID);
                db.NotificationList.Attach(l);
                db.NotificationList.Remove(l);
                db.SaveChanges();
                ar.Type.Add(1);
                ar.Messages.Add(Resources.AnswerResultSuccess);
            }
            catch (DbEntityValidationException E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }

            }
            return Json(ar);
        }
        public JsonResult ServerFilteringGetEmployee(string text, int NotificationListID)
        {
            var employeesList = db.NotificationsEmployeeDestination.Where(a => a.NotificationListID == NotificationListID).Select(a => a.EmployeeID).ToList();
            var products = db.Employees.Where(p => p.EmployeeFullName.Contains(text) && !employeesList.Contains(p.EmployeeID) ).Select(x => new EmployeeViewModel
            {
                Value = x.EmployeeID,
                Text = x.EmployeeFullName
            });
            return Json(products, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ServerFilteringGetEmployeeWO(string text)
        {
            var products = db.Employees.Where(p => p.EmployeeFullName.Contains(text)&&p.EmployeeStatus==true).Select(x => new EmployeeViewModel
            {
                Value = x.EmployeeID,
                Text = x.EmployeeFullName
            });
            return Json(products, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ServerFilteringGetDepartmentWO(string text)
        {
            var products = db.Departments.Where(p => p.DepartmentName.Contains(text)).Select(x => new 
            {
                Value = x.DepartmentID,
                Text = x.DepartmentName
            });
            return Json(products, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ServerFilteringGetJobTitleWO(string text)
        {
            var products = db.JobTitles.Where(p => p.JobTitleName.Contains(text)).Select(x => new EmployeeViewModel
            {
                Value = x.JobTitleID,
                Text = x.JobTitleName
            });
            return Json(products, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReadNotificationEmployeeDestination([DataSourceRequest] DataSourceRequest request, int NotificationListID)
        {
            var list = db.NotificationsEmployeeDestination.Where(a => a.NotificationListID == NotificationListID).Select(s => new
            {
                NotificationsEmployeeDestinationID = s.NotificationsEmployeeDestinationID,
                NotificationListID = s.NotificationListID,
                EmployeeID = s.EmployeeID,
                Employees = new 
                {
                    EmployeeID = s.EmployeeID,
                    EmployeeNameViewChar = s.Employees.EmployeeFullName
                }
            }).ToList();
            return Json(list.ToDataSourceResult(request));
        }
        [HttpPost]
        public ActionResult AddNotificationEmployeeDestination([DataSourceRequest] DataSourceRequest request, NotificationsEmployeeDestination ned)
        {
            ModelState["Employees.EmployeeStartDate"].Errors.Clear();
            db.Configuration.LazyLoadingEnabled = false;
            ned.Employees = null;
            ned.NotificationList = null;
            db.NotificationsEmployeeDestination.Add(ned);
            Models.Employees emp = db.Employees.FirstOrDefault(a => a.EmployeeID == ned.EmployeeID);
            db.SaveChanges();
            var need = new
            {
                NotificationsEmployeeDestinationID = ned.NotificationsEmployeeDestinationID,
                NotificationListID = ned.NotificationListID,
                EmployeeID = ned.EmployeeID,
                Employees = new
                {
                    EmployeeID = emp.EmployeeID,
                    EmployeeNameViewChar = emp.EmployeeFullName
                },
                NotificationList = new { NotificationListID = ned.NotificationListID }
            };
            return Json(new[] { need }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult DeleteNotificationEmployeeDestination([DataSourceRequest] DataSourceRequest request, NotificationsEmployeeDestination ned)
        {
            //ModelState["Employees.EmployeeStartDate"].Errors.Clear();
            /*  TemplateDemo.Models.EmployeeModulePermission emp = new TemplateDemo.Models.EmployeeModulePermission();
              emp.EmployeeID = pevm.EmployeeID;
              emp.ModuleID = pevm.ModuleID;
              emp.AccessP = pevm.AccessP;
              emp.CreateP = pevm.CreateP;
              emp.ReadP = pevm.ReadP;
              emp.UpdateP = pevm.UpdateP;
              emp.DeleteP = pevm.DeleteP;
              emp.PrintP = pevm.PrintP;*/
            NotificationsEmployeeDestination nedDelete = db.NotificationsEmployeeDestination.FirstOrDefault(a => a.NotificationsEmployeeDestinationID == ned.NotificationsEmployeeDestinationID);
                db.NotificationsEmployeeDestination.Remove(nedDelete);
                db.SaveChanges();

            return Json(new[] { ned }.ToDataSourceResult(request, ModelState));
        }
    }
}