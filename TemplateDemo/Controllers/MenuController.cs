﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TemplateDemo.Models;
using BAEClassLibrary;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Net;
using System.Text;


namespace TemplateDemo.Controllers
{
    public class MenuController : BAEController
    {
        BAETDDB db = new BAETDDB();
        // GET: Menu
        public ActionResult Index()
        {
            ViewBag.Apps = db.Menu.GroupBy(a => a.AppName).Select(c => new SelectListItem
            {
                Value = c.Key,
                Text = c.Key
            }).OrderBy(a => a.Text);
            ViewBag.Department = db.Menu.GroupBy(a => a.MenuDepartment).Select(c => new SelectListItem
            {
                Value = c.Key,
                Text = c.Key
            }).OrderBy(a => a.Text);
            return View();
        }
        public ActionResult ShowMenu(string DepartmentName)
        {
            return PartialView(db.Menu.Where(a => a.DadMenu == null && a.MenuDepartment == DepartmentName).OrderBy(a => a.SubMenuOrder).ToList());

        }
        [HttpPost]
        public ActionResult OnDrop(int DropedID, int ReferenceID, int? DadID, string Position, string Department)
        {
            
            AnswerResult ar = new AnswerResult();
            try
            {
                int i = 1;
                bool changed = false;
                TemplateDemo.Models.Menu m = null;
                if (Position == "over")
                {
                    m = db.Menu.Where(a => a.MenuID == DropedID).First();
                    m.DadMenu = ReferenceID;
                    m.SubMenuOrder = db.Menu.Where(a => a.DadMenu == ReferenceID).Max(a => a.SubMenuOrder)!=null? db.Menu.Where(a => a.DadMenu == ReferenceID).Max(a => a.SubMenuOrder) + 1:1;
                    db.Menu.Attach(m);
                    db.Entry(m).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    List<TemplateDemo.Models.Menu> menus = db.Menu.Where(a => a.DadMenu == (DadID == 0 ? null : DadID) && a.MenuDepartment == Department).OrderBy(a => a.SubMenuOrder).ToList();
                    foreach (var item in menus)
                    {
                        if (!changed && Position == "before" && item.MenuID == ReferenceID)
                        {
                            m = db.Menu.Where(a => a.MenuID == DropedID).First();
                            m.SubMenuOrder = i;
                            m.DadMenu = (DadID == 0 ? null : DadID);
                            db.Menu.Attach(m);
                            db.Entry(m).State = EntityState.Modified;
                            db.SaveChanges();
                            i++;
                            changed = true;
                        }
                        //cambio
                        if (DropedID != item.MenuID)
                        {
                            item.SubMenuOrder = i;
                            db.Menu.Attach(item);
                            db.Entry(item).State = EntityState.Modified;
                            db.SaveChanges();
                            i++;
                        }

                        if (!changed && Position == "after" && item.MenuID == ReferenceID)
                        {
                            m = db.Menu.Where(a => a.MenuID == DropedID).First();
                            m.SubMenuOrder = i;
                            m.DadMenu = (DadID == 0 ? null : DadID);
                            db.Menu.Attach(m);
                            db.Entry(m).State = EntityState.Modified;
                            db.SaveChanges();
                            i++;
                            changed = true;
                        }
                    }
                }
                ar.Messages.Add(Resources.AnswerResultSuccess);
                ar.Type.Add(1);
            }
            catch (Exception E)
            {
                ar.Messages.Add(Resources.AnswerResultError);
                ar.Type.Add(3);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                ar.Type.Add(3);
            }
            return Json(ar);
        }
        [HttpPost]
        public ActionResult OnCheck(int DropedID, bool active)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                TemplateDemo.Models.Menu m = db.Menu.Where(a => a.MenuID == DropedID).First();
                m.Active = active;
                db.Menu.Attach(m);
                db.Entry(m).State = EntityState.Modified;
                db.SaveChanges();
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultSuccess);
                ar.Type.Add(1);
            }
            catch (Exception E)
            {
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                ar.Type.Add(3);
            }
            return Json(ar);

        }
        [HttpPost]
        public ActionResult CreateMenu(int MenuID, string MenuNameEn, string MenuNameSp, string MenuHref, string MenuIcon, string AppName, string MenuDepartment)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                TemplateDemo.Models.Menu m = new TemplateDemo.Models.Menu()
                {
                    MenuNameEn = MenuNameEn,
                    MenuNameSp = MenuNameSp,
                    MenuHref = MenuHref,
                    MenuIcon = MenuIcon,
                    DadMenu = null,
                    AppName = AppName,
                    MenuDepartment = MenuDepartment,
                    Active = false,
                    SubMenuOrder = db.Menu.Where(a => a.MenuDepartment == MenuDepartment) == null ? 1 : db.Menu.Where(a => a.MenuDepartment == MenuDepartment).Max(a => a.SubMenuOrder) + 1
                };
                db.Menu.Add(m);
                db.SaveChanges();
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultSuccess);
                ar.Type.Add(1);
            }
            catch (DbEntityValidationException E)
            {
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                ar.Type.Add(3);
            }
            return Json(ar);
        }
        public ActionResult EditMenu(int MenuID, string MenuNameEn, string MenuNameSp, string MenuHref, string MenuIcon, string AppName, string MenuDepartment)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                TemplateDemo.Models.Menu m = db.Menu.FirstOrDefault(a => a.MenuID == MenuID);

                m.MenuNameEn = MenuNameEn;
                m.MenuNameSp = MenuNameSp;
                m.MenuHref = MenuHref;
                m.MenuIcon = MenuIcon;
              
                if (m.AppName!=AppName)
                {
                    m.AppName = AppName;
                    TemplateDemo.Models.Menu mm = db.Menu.Where(a => a.AppName == AppName && a.DadMenu == null).FirstOrDefault();
                    if (mm!=null)
                    {
                        m.DadMenu = mm.MenuID;
                    }
                    else
                    {
                        m.DadMenu = null;
                    }
                    
                    m.SubMenuOrder = -1;
                }
                m.MenuDepartment = MenuDepartment;


                db.Menu.Attach(m);
                db.Entry(m).State = EntityState.Modified;
                db.SaveChanges();
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultSuccess);
                ar.Type.Add(1);
            }
            catch (DbEntityValidationException E)
            {
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                ar.Type.Add(3);
            }
            return Json(ar);
        }
        public ActionResult DeleteMenu(int MenuID)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                TemplateDemo.Models.Menu m = db.Menu.FirstOrDefault(a => a.MenuID == MenuID);
                db.Database.ExecuteSqlCommand("Delete from API.MenuAccess where MenuID = "+ m.MenuID.ToString());
                


                db.Menu.Attach(m);
                db.Menu.Remove(m);
                db.SaveChanges();
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultSuccess);
                ar.Type.Add(1);
            }
            catch (DbEntityValidationException E)
            {
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                ar.Type.Add(3);
            }
            return Json(ar);
        }
        [HttpPost]
        public ActionResult GetMenu(int MenuID)
        {
            var m = db.Menu.Where(a => a.MenuID == MenuID).Select(x => new
            {
                MenuNameEn = x.MenuNameEn,
                MenuNameSp = x.MenuNameSp,
                MenuHref = x.MenuHref,
                MenuIcon = x.MenuIcon,
                AppName = x.AppName,
                MenuDepartment = x.MenuDepartment

            }).FirstOrDefault();
            return Json(m);
        }
        public ActionResult GetPermisionForm(int MenuID)
        {
            string uri = "https://apps.bae.gym:84/BAEAPI/Menu/";
            var wb = new WebClient();
            wb.Encoding = Encoding.UTF8;
            List<string> MenuAccessList = db.MenuAccess.Where(a => a.MenuID == MenuID).Select(a => a.GroupName).ToList();
            List<string> Groups = new List<string>(wb.DownloadString(uri + "GetActiveDirectoryGroups").Split(','));
            ViewBag.AvailableGroups = Groups.Except(MenuAccessList).Select(c => new SelectListItem
            {
                Value = c.ToString(),
                Text = c.ToString()
            }).OrderBy(a => a.Text);
            ViewBag.AccessGroups = MenuAccessList.Select(c => new SelectListItem
            {
                Value = c.ToString(),
                Text = c.ToString()
            }).OrderBy(a => a.Text);
            return PartialView();
        }
        public ActionResult GetAvailableGroups(int MenuID)
        {
            string uri = "https://apps.bae.gym:84/BAEAPI/Menu/";
            //string uri = "https://localhost:44317/Menu/";
            var wb = new WebClient();
            wb.Encoding = Encoding.UTF8;
            List<string> MenuAccessList = db.MenuAccess.Where(a => a.MenuID == MenuID).Select(a => a.GroupName).ToList();
            List<string> Groups = new List<string>(wb.DownloadString(uri + "GetActiveDirectoryGroups").Split(','));
            var AvailableGroups = Groups.Except(MenuAccessList).Select(c => new PermissionViewModel
            {
                Id = 0,
                Permission = c.ToString()
            }).OrderBy(a => a.Permission);

            return Json(AvailableGroups, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAvailablePermissions(int MenuID)
        {
            List<string> MenuAccessList = db.MenuAccess.Where(a => a.MenuID == MenuID).Select(a => a.GroupName).ToList();
            var AvailableGroups = db.MenuAccess.Where(a => a.MenuID == MenuID).Select(c => new PermissionViewModel
            {
                Id = c.MenuAccessID,
                Permission = c.GroupName
            }).OrderBy(a => a.Permission);

            return Json(AvailableGroups, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateMenuAccess(int MenuId, string ListName, List<PermissionViewModel> pvm)
        {
            AnswerResult ar = new AnswerResult();
            ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultSuccess);
            ar.Type.Add(1);
            try
            {
                foreach (var item in pvm)
                {
                    if (ListName == "GroupsList")//borrar
                    {
                        MenuAccess ma = db.MenuAccess.FirstOrDefault(a => a.MenuAccessID == item.Id);
                        if (ma != null)
                        {
                            db.MenuAccess.Remove(ma);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        MenuAccess ma = new MenuAccess() { MenuID = MenuId, GroupName = item.Permission };
                        if (ma != null)
                        {
                            db.MenuAccess.Add(ma);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception E)
            {
                ar.Messages.Clear();
                ar.Type.Clear();
                ar.Messages.Add(BAEClassLibrary.Resources.AnswerResultError);
                ar.Type.Add(3);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                ar.Type.Add(3);
            }
            return Json(ar);
        }
    }
}