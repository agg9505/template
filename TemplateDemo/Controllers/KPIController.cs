﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TemplateDemo.Models;
using BAEClassLibrary;
using Newtonsoft.Json;
using System.Data.Entity.Validation;
using System.Data.Entity;

namespace TemplateDemo.Controllers
{
    public class KPIController : BAEController
    {
        BAETDDB db = new BAETDDB();
        // GET: KPI
        public ActionResult Index()
        {
             AnswerResult ar = (AnswerResult)TempData["AnswerResult"] ?? new AnswerResult(); TempData["AnswerResult"] = null;
            ViewBag.AnswerResult = JsonConvert.SerializeObject(ar);
            return View();
        }
        public ActionResult ReadKPIS([DataSourceRequest] DataSourceRequest request)
        {
            IList<KPIViewModel> list = db.KPIS.Select(s=> new KPIViewModel()
            {
                 KPIID =s.KPIID,
                 AppName =s.AppName,
                 KPIName =s.KPIName,
                 KPIDescription =s.KPIDescription,
                 Active =s.Active,
                 Url =s.Url
            }).ToList();
            return Json(list.ToDataSourceResult(request));
        }
        public ActionResult Detail(int? id)
        {
            AnswerResult ar = (AnswerResult)TempData["AnswerResult"] ?? new AnswerResult(); TempData["AnswerResult"] = null;
            KPIS eq = id != null && id != 0 ? db.KPIS.FirstOrDefault(a => a.KPIID == id) : new KPIS() { Active = true };
            ViewBag.Action = eq.KPIID != 0 ? "Edit" : "Create";
            ViewBag.AppName = db.Menu.Select(a=>a.AppName).Distinct().Select(c => new SelectListItem
            {
                Value = c.ToString(),
                Text =c.ToString()
            }).OrderBy(a => a.Text);
            ViewBag.AnswareResult = JsonConvert.SerializeObject(ar);
            return View(eq);
        }
        [HttpPost]
        public ActionResult Create(KPIS kpi)
        {
            AnswerResult ar = new AnswerResult();
            if (ModelState.IsValid)
            {
                try
                {
                    db.KPIS.Add(kpi);
                    db.SaveChanges();
                    ar.Type.Add(1);
                    ar.Messages.Add(Resources.AnswerResultSuccess);
                    ar.ID = kpi.KPIID;
                }
                catch (DbEntityValidationException E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    foreach (var eve in E.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ar.Type.Add(3);
                            ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                        }
                    }
                }
                catch (Exception E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    if (E.InnerException != null)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                    else
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }

                }
            }
            return Json(ar);
        }
        [HttpPost]
        public ActionResult Edit(KPIS kpi)
        {
            AnswerResult ar = new AnswerResult();
            if (ModelState.IsValid)
            {
                try
                {
                    db.KPIS.Attach(kpi);
                    db.Entry(kpi).State = EntityState.Modified;
                    db.SaveChanges();
                    ar.Type.Add(1);
                    ar.ID = kpi.KPIID;
                    ar.Messages.Add(Resources.AnswerResultSuccess);
                }
                catch (DbEntityValidationException E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    foreach (var eve in E.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ar.Type.Add(3);
                            ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                        }
                    }
                }
                catch (Exception E)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(Resources.AnswerResultError);
                    if (E.InnerException != null)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                    else
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }

                }
            }

            return Json(ar);
        }
        public ActionResult ShowDeleteConfirmation(int id)
        {
            KPIS eq = db.KPIS.FirstOrDefault(a => a.KPIID == id);
            //verificamos que no tenga dependencias 
            ViewBag.EntryFound = false;
            if (eq.DashboardUser.Count > 0)
            {
                ViewBag.EntryFound = true;
            }
            return PartialView(eq);
        }
        [HttpPost]
        public ActionResult Delete(KPIS kpi)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                KPIS k = db.KPIS.FirstOrDefault(a => a.KPIID == kpi.KPIID);
                db.KPIS.Attach(k);
                db.KPIS.Remove(k);
                db.SaveChanges();
                ar.Type.Add(1);
                ar.Messages.Add(Resources.AnswerResultSuccess);
            }
            catch (DbEntityValidationException E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }

            }
            return Json(ar);
        }
        public ActionResult MyDashboard()
        {
            AnswerResult ar = new AnswerResult();
            ViewBag.AnswareResult = JsonConvert.SerializeObject(ar);
            return View(db.DashboardUser.Where(a => a.EmployeeID == UserContext.current.EmployeeID).ToList());
        }
        [HttpPost]
        public ActionResult MyDashboardResize(List<predashboard> listDash)
        {
            AnswerResult ar = new AnswerResult();
            ar.Messages.Add(Resources.AnswerResultSuccess);
            ar.Type.Add(1);
            foreach (var item in listDash)
            {
                DashboardUser dh = db.DashboardUser.FirstOrDefault(a => a.DashboardID == item.DashboardID);
                if (dh != null)
                {
                    try
                    {
                        dh.datax = item.x;
                        dh.datay = item.y;
                        dh.datah = item.h;
                        dh.dataw = item.w;
                        db.Entry(dh).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        ar.Messages.Clear();
                        ar.Type.Clear();
                        ar.Messages.Add(Resources.AnswerResultError);
                        ar.Type.Add(3);
                        ar.Messages.Add(ex.ToString());
                        ar.Type.Add(3);
                    }
                }

            }

            return Json(ar);
        }
        [HttpPost]
        public ActionResult MyDashboardDelete(int DashboardID)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                DashboardUser dh = db.DashboardUser.FirstOrDefault(a => a.DashboardID == DashboardID);
                if (dh != null)
                {

                    db.DashboardUser.Attach(dh);
                    db.DashboardUser.Remove(dh);
                    db.SaveChanges();
                    ar.Messages.Add(Resources.AnswerResultSuccess);
                    ar.Type.Add(1);
                }
            }
            catch (Exception ex)
            {
                ar.Messages.Add(Resources.AnswerResultError);
                ar.Type.Add(3);
                ar.Messages.Add(ex.ToString());
                ar.Type.Add(3);
            }

            return Json(ar);
        }
        public ActionResult AddKPI(int kpiid)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                DashboardUser du = new DashboardUser()
                {
                    dataw = 3,
                    datah = 3,

                    datax = 0,
                    KPIID = kpiid,
                    EmployeeID = UserContext.current.EmployeeID
                };
                try
                {
                    du.datay = db.DashboardUser.Where(a => a.EmployeeID == UserContext.current.EmployeeID).Max(a => a.datay + a.dataw) + 1;
                }
                catch (Exception)
                {

                    du.datay = 0;
                }
                db.DashboardUser.Add(du);
                db.SaveChanges();
                ar.ID = du.DashboardID;
                ar.Type.Add(1);
                ar.Messages.Add(Resources.AnswerResultSuccess);
                ar.Text = db.KPIS.First(a => a.KPIID == kpiid).Url;
            }
            catch (Exception ex)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                ar.Type.Add(3);
                ar.Messages.Add(ex.ToString());
                throw;
            }
            return Json(ar);
        }
        public ActionResult ReadKPIList()
        {
            var kpis = db.KPIS.Where(a => a.Active).ToList().Select(c => new KPIListViewModel
            {
                ID = c.KPIID,
                Name = c.KPIName,
                App = c.AppName,
                Desc = c.KPIDescription
            }).OrderBy(a => a.App).ThenBy(a=>a.Name).ToList();
            return Json(kpis, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DashboardHeader()
        {
            return PartialView();
        }
 
    }
}