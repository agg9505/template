﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TemplateDemo.Models;
using BAEClassLibrary;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Net;
using System.Text;

namespace TemplateDemo.Controllers
{
    public class PermissionController : BAEController
    {
        BAETDDB db = new BAETDDB();
        // GET: Permission
        [AccessFilter(Module = "PermissionManager")]
        public ActionResult Index(string ModuleAdmin)
        {
            AnswerResult ar = new AnswerResult();
            ViewBag.AnswerResult = JsonConvert.SerializeObject(ar);
            if (!String.IsNullOrEmpty(ModuleAdmin))
            {
                if (ModuleAdmin.Contains("App"))
                {
                    if (UserContext.FindPermision(ModuleAdmin, "Access"))
                    {
                        string app = ModuleAdmin.Replace("App", "");
                        ViewBag.Apps = db.Apps.Where(a=>a.AppName==app).Select(c => new SelectListItem
                        {
                            Value = c.AppID.ToString(),
                            Text = c.AppName
                        }).OrderBy(a => a.Text);
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            else
            {
                if (UserContext.current.IsAdmin)
                {
                    ViewBag.Apps = db.Apps.Select(c => new SelectListItem
                    {
                        Value = c.AppID.ToString(),
                        Text = c.AppName
                    }).OrderBy(a => a.Text);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            
            return View();
        }
        public JsonResult ServerFilteringGetEmployee(string text)
        {
            var products = db.Employees.Where(p => p.EmployeeFullName.Contains(text)).Select(x => new EmployeeViewModel
            {
                Value = x.EmployeeID,
                Text = x.EmployeeFullName
            });
            return Json(products, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProfileByApp(int App)
        {
            return Json(db.Profile.Where(p => p.AppID == App).Select(x => new EmployeeViewModel
            {
                Value = x.ProfileID,
                Text = x.Name
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ProfileByEmployeeAndApp(int EmployeeID, int App)
        {
            TemplateDemo.Models.EmployeeProfile ep = db.EmployeeProfile.Where(p => p.EmployeeID == EmployeeID && p.Profile.AppID == App).FirstOrDefault();
            if (ep == null)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(ep.ProfileID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ProfileByApp(int App)
        {
            TemplateDemo.Models.EmployeeProfile ep = db.EmployeeProfile.Where(p => p.Profile.AppID == App).FirstOrDefault();
            if (ep == null)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(ep.ProfileID, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPermissionByEmployee([DataSourceRequest] DataSourceRequest request, int EmployeeID, int App)
        {
            IList<PermissionEmployeeViewModel> list = db.EmployeeModulePermission.Where(a => a.EmployeeID == EmployeeID && a.Modules.AppID == App).Select(s => new PermissionEmployeeViewModel()
            {
                PermissionID = s.PermissionID,
                ModuleID = s.ModuleID,
                Module = new ModuleViewModel()
                {
                    ModuleID = s.ModuleID,
                    ModuleName = s.Modules.Module
                },
                AccessP = s.AccessP,
                CreateP = s.CreateP,
                ReadP = s.ReadP,
                UpdateP = s.UpdateP,
                DeleteP = s.DeleteP,
                PrintP = s.PrintP,
                EmployeeID = s.EmployeeID
            }).ToList();
            return Json(list.ToDataSourceResult(request));
        }
        [HttpPost]
        public ActionResult UpdatePermission([DataSourceRequest] DataSourceRequest request, PermissionEmployeeViewModel pevm)
        {
            if (pevm != null && ModelState.IsValid)
            {
                if (db.EmployeeModulePermission.FirstOrDefault(a => a.PermissionID == pevm.PermissionID) == null)
                {
                    ModelState.AddModelError("PermissionID", Resources.Research + ", " + Resources.NotFound);
                }
                else
                {
                    TemplateDemo.Models.EmployeeModulePermission emp = db.EmployeeModulePermission.FirstOrDefault(a => a.PermissionID == pevm.PermissionID);
                    emp.AccessP = pevm.AccessP;
                    emp.CreateP = pevm.CreateP;
                    emp.ReadP = pevm.ReadP;
                    emp.UpdateP = pevm.UpdateP;
                    emp.DeleteP = pevm.DeleteP;
                    emp.PrintP = pevm.PrintP;
                    db.EmployeeModulePermission.Attach(emp);
                    db.Entry(emp).State = EntityState.Modified;
                    db.SaveChanges();
                    //find app by module
                    FindProfileAndDeleteIT(emp);
                }
            }
            return Json(new[] { pevm }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult AddPermission([DataSourceRequest] DataSourceRequest request, PermissionEmployeeViewModel pevm)
        {
            if (pevm != null && ModelState.IsValid)
            {
                TemplateDemo.Models.EmployeeModulePermission emp = new TemplateDemo.Models.EmployeeModulePermission();
                emp.EmployeeID = pevm.EmployeeID;
                emp.ModuleID = pevm.ModuleID;
                emp.AccessP = pevm.AccessP;
                emp.CreateP = pevm.CreateP;
                emp.ReadP = pevm.ReadP;
                emp.UpdateP = pevm.UpdateP;
                emp.DeleteP = pevm.DeleteP;
                emp.PrintP = pevm.PrintP;
                 db.EmployeeModulePermission.Add(emp);
                db.SaveChanges();
                pevm.PermissionID = emp.PermissionID;
                //find app by module
                FindProfileAndDeleteIT(emp);
            }
            return Json(new[] { pevm }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult GetPermissionByProfile([DataSourceRequest] DataSourceRequest request, int ProfileID, int App)
        {
            IList<PermissionProfileViewModel> list = db.ProfilePermission.Where(a => a.ProfileID == ProfileID && a.Modules.AppID == App).Select(s => new PermissionProfileViewModel()
            {
                PermissionID = s.ProfilePermissionID,
                ModuleID = s.ModuleID,
                Module = new ModuleViewModel()
                {
                    ModuleID = s.ModuleID,
                    ModuleName = s.Modules.Module
                },
                AccessP = s.AccessP,
                CreateP = s.CreateP,
                ReadP = s.ReadP,
                UpdateP = s.UpdateP,
                DeleteP = s.DeleteP,
                PrintP = s.PrintP,
                ProfileID = s.ProfileID
            }).ToList();
            return Json(list.ToDataSourceResult(request));
        }
        [HttpPost]
        public ActionResult UpdatePermissionProfile([DataSourceRequest] DataSourceRequest request, PermissionProfileViewModel ppvm)
        {
            if (ppvm != null && ModelState.IsValid)
            {
                if (db.ProfilePermission.FirstOrDefault(a => a.ProfilePermissionID == ppvm.PermissionID) == null)
                {
                    ModelState.AddModelError("PermissionID", Resources.Research + ", " + Resources.NotFound);
                }
                else
                {
                    TemplateDemo.Models.ProfilePermission emp = db.ProfilePermission.FirstOrDefault(a => a.ProfilePermissionID == ppvm.PermissionID);
                    emp.AccessP = ppvm.AccessP;
                    emp.CreateP = ppvm.CreateP;
                    emp.ReadP = ppvm.ReadP;
                    emp.UpdateP = ppvm.UpdateP;
                    emp.DeleteP = ppvm.DeleteP;
                    emp.PrintP = ppvm.PrintP;
                    db.ProfilePermission.Attach(emp);
                    db.Entry(emp).State = EntityState.Modified;
                    db.SaveChanges();
                    //find all the employees in the profile
                    var employeeList = db.EmployeeProfile.Where(a => a.ProfileID == emp.ProfileID).Select(a => a.EmployeeID).ToList();
                    //find all modules per employee in the profile and update
                    var emPr = db.EmployeeModulePermission.Where(a => a.ModuleID == emp.ModuleID && employeeList.Contains(a.EmployeeID)).ToList();
                    foreach (var item in emPr)
                    {
                        item.AccessP = emp.AccessP;
                        item.CreateP = emp.CreateP;
                        item.ReadP = emp.ReadP;
                        item.UpdateP = emp.UpdateP;
                        item.DeleteP = emp.DeleteP;
                        item.PrintP = emp.PrintP;
                        db.EmployeeModulePermission.Attach(item);
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            return Json(new[] { ppvm }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult AddPermissionProfile([DataSourceRequest] DataSourceRequest request, PermissionProfileViewModel ppvm)
        {
            if (ppvm != null && ModelState.IsValid)
            {
                TemplateDemo.Models.ProfilePermission emp = new TemplateDemo.Models.ProfilePermission();
                emp.ProfileID = ppvm.ProfileID;
                emp.ModuleID = ppvm.ModuleID;
                emp.AccessP = ppvm.AccessP;
                emp.CreateP = ppvm.CreateP;
                emp.ReadP = ppvm.ReadP;
                emp.UpdateP = ppvm.UpdateP;
                emp.DeleteP = ppvm.DeleteP;
                emp.PrintP = ppvm.PrintP;
                db.ProfilePermission.Add(emp);
                db.SaveChanges();
                ppvm.PermissionID = emp.ProfilePermissionID;
                //add new module to all employees in the profile
                var emPr = db.EmployeeProfile.Where(a => a.ProfileID == emp.ProfileID).ToList();
                foreach (var item in emPr)
                {
                    TemplateDemo.Models.EmployeeModulePermission empp = new TemplateDemo.Models.EmployeeModulePermission();
                    empp.EmployeeID = item.EmployeeID;
                    empp.ModuleID = emp.ModuleID;
                    empp.AccessP = emp.AccessP;
                    empp.CreateP = emp.CreateP;
                    empp.ReadP = emp.ReadP;
                    empp.UpdateP = emp.UpdateP;
                    empp.DeleteP = emp.DeleteP;
                    empp.PrintP = emp.PrintP;
                    db.EmployeeModulePermission.Add(empp);
                    db.SaveChanges();
                }
            }
            return Json(new[] { ppvm }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult GetEmployeesByProfile([DataSourceRequest] DataSourceRequest request, int ProfileID)
        {
            IList<EmployeeViewModel> list = db.EmployeeProfile.Where(a => a.ProfileID == ProfileID).Select(s => new EmployeeViewModel()
            {
                Value = s.EmployeeProfileID,
                Text = s.Employees.EmployeeNameViewChar
            }).ToList();
            return Json(list.ToDataSourceResult(request));
        }
        public ActionResult AddProfileToEmployee(int ProfileID, int EmployeeID)
        {
            AnswerResult ar = new AnswerResult();
            try
            {

                TemplateDemo.Models.EmployeeProfile emp = new TemplateDemo.Models.EmployeeProfile();
                emp.EmployeeID = EmployeeID;
                emp.ProfileID = ProfileID;
                db.EmployeeProfile.Add(emp);
                db.SaveChanges();
                ar.Type.Add(1);
                ar.Messages.Add(Resources.AnswerResultSuccess);
                var modulesByProfile = db.ProfilePermission.Where(a => a.ProfileID == emp.ProfileID).ToList();
                TemplateDemo.Models.Profile P = db.Profile.Where(a => a.ProfileID == ProfileID).FirstOrDefault();
                var employeeModule = db.EmployeeModulePermission.Where(a => a.EmployeeID == emp.EmployeeID && a.Modules.AppID == P.AppID).ToList();
                if (employeeModule != null)
                {
                    db.EmployeeModulePermission.RemoveRange(employeeModule);
                    db.SaveChanges();
                }
                foreach (var item in modulesByProfile)
                {
                    TemplateDemo.Models.EmployeeModulePermission empp = new TemplateDemo.Models.EmployeeModulePermission();
                    empp.EmployeeID = EmployeeID;
                    empp.ModuleID = item.ModuleID;
                    empp.AccessP = item.AccessP;
                    empp.CreateP = item.CreateP;
                    empp.ReadP = item.ReadP;
                    empp.UpdateP = item.UpdateP;
                    empp.DeleteP = item.DeleteP;
                    empp.PrintP = item.PrintP;
                    db.EmployeeModulePermission.Add(empp);
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
            }
            return Json(ar);
        }
        public ActionResult DeleteProfileToEmployee(int ProfileID, int EmployeeID)
        {
            AnswerResult ar = new AnswerResult();
            try
            {

                TemplateDemo.Models.EmployeeProfile ep = db.EmployeeProfile.Where(p => p.EmployeeID == EmployeeID && p.ProfileID==ProfileID).FirstOrDefault();
                //find all modules per employee in the profile and update
                /*   var modulesByProfile = db.ProfilePermission.Where(a => a.ProfileID == ep.ProfileID).ToList();
                   TemplateDemo.Models.Profile P = db.Profile.Where(a => a.ProfileID == ep.ProfileID).FirstOrDefault();
                   var employeeModule = db.EmployeeModulePermission.Where(a => a.EmployeeID == ep.EmployeeID && a.Modules.AppID == P.AppID).ToList();
                   if (employeeModule != null)
                   {
                      // db.EmployeeModulePermission.RemoveRange(employeeModule);

                   }*/
                db.EmployeeProfile.Remove(ep);
                db.SaveChanges();
                ar.Messages.Add(Resources.AnswerResultSuccess);
                ar.Type.Add(1);
            }
            catch (DbEntityValidationException E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
            }
            return Json(ar);
        }
        public void FindProfileAndDeleteIT(TemplateDemo.Models.EmployeeModulePermission emp)
        {
            TemplateDemo.Models.Modules moduleApp = db.Modules.FirstOrDefault(a => a.ModuleID == emp.ModuleID);
            TemplateDemo.Models.EmployeeProfile empro = db.EmployeeProfile.Where(a => a.EmployeeID == emp.EmployeeID && a.Profile.AppID == moduleApp.AppID).FirstOrDefault();
            if (empro != null)
            {
                db.EmployeeProfile.Attach(empro);
                db.EmployeeProfile.Remove(empro);
                db.SaveChanges();
            }
        }
        public ActionResult DeleteEmployeeFromProfile(int EmployeeProfileID)
        {
            AnswerResult ar = new AnswerResult();
            try
            {
                TemplateDemo.Models.EmployeeProfile ep = db.EmployeeProfile.Where(p => p.EmployeeProfileID == EmployeeProfileID).FirstOrDefault();
                //find all modules per employee in the profile and update
                var modulesByProfile = db.ProfilePermission.Where(a => a.ProfileID == ep.ProfileID).ToList();
                TemplateDemo.Models.Profile P = db.Profile.Where(a => a.ProfileID == ep.ProfileID).FirstOrDefault();
                var employeeModule = db.EmployeeModulePermission.Where(a => a.EmployeeID == ep.EmployeeID && a.Modules.AppID == P.AppID).ToList();
                if (employeeModule != null)
                {
                    db.EmployeeModulePermission.RemoveRange(employeeModule);
                    db.EmployeeProfile.Remove(ep);
                    db.SaveChanges();
                }
                ar.Type.Add(1);
                ar.Messages.Add(Resources.AnswerResultSuccess);
            }
            catch (DbEntityValidationException E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
            }
            return Json(ar);
           
        }
        public ActionResult GetAvailableModulesByAppAndEmployee(int EmployeeID, int App,int ModuleID)
        {
            //find all modules per employee in the profile and update
          
            if (ModuleID!=0)
            {
                var availableModules = db.Modules.Where(a => a.AppID == App && a.ModuleID==ModuleID).Select(s => new ModuleViewModel()
                {
                    ModuleID = s.ModuleID,
                    ModuleName = s.Module
                }).ToList();
                return Json(availableModules, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var emPr = db.EmployeeModulePermission.Where(a => a.Modules.AppID == App && a.EmployeeID == EmployeeID).Select(a => a.ModuleID).ToList();
                var availableModules = db.Modules.Where(a => a.AppID == App && !emPr.Contains(a.ModuleID)).Select(s => new ModuleViewModel()
                {
                    ModuleID = s.ModuleID,
                    ModuleName = s.Module
                }).ToList();
                return Json(availableModules, JsonRequestBehavior.AllowGet);
            }
          
        }
        public ActionResult GetAvailableModulesByAppAndProfile(int ProfileID, int App, int ModuleID)
        {
            //find all modules per employee in the profile and update

            if (ModuleID != 0)
            {
                var availableModules = db.Modules.Where(a => a.AppID == App && a.ModuleID == ModuleID).Select(s => new ModuleViewModel()
                {
                    ModuleID = s.ModuleID,
                    ModuleName = s.Module
                }).ToList();
                return Json(availableModules, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var emPr = db.ProfilePermission.Where(a => a.Modules.AppID == App && a.ProfileID == ProfileID).Select(a => a.ModuleID).ToList();
                var availableModules = db.Modules.Where(a => a.AppID == App && !emPr.Contains(a.ModuleID)).Select(s => new ModuleViewModel()
                {
                    ModuleID = s.ModuleID,
                    ModuleName = s.Module
                }).ToList();
                return Json(availableModules, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeletePermissionFromProfile(int ProfilePermissionID)
        {
            AnswerResult ar = new AnswerResult();
            try
            {

                TemplateDemo.Models.ProfilePermission pp = db.ProfilePermission.Where(p => p.ProfilePermissionID == ProfilePermissionID).FirstOrDefault();
                //find all modules per employee in the profile and update
                var ep = db.EmployeeProfile.Where(a => a.ProfileID == pp.ProfileID).Select(a=>a.EmployeeID).ToList();
                var employeeModule = db.EmployeeModulePermission.Where(a => a.ModuleID == pp.ModuleID && ep.Contains( a.EmployeeID)).ToList();
                if (employeeModule != null)
                {
                    db.EmployeeModulePermission.RemoveRange(employeeModule);
                    
                }
                db.ProfilePermission.Remove(pp);
                db.SaveChanges();
                ar.Type.Add(1);
                ar.Messages.Add(Resources.AnswerResultSuccess);
            }
            catch (DbEntityValidationException E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
            }
            return Json(ar);

        }
        public ActionResult DeletePermissionFromEmployee(int PermissionID)
        {
            AnswerResult ar = new AnswerResult();
            try
            {

                TemplateDemo.Models.EmployeeModulePermission emp = db.EmployeeModulePermission.FirstOrDefault(a => a.PermissionID == PermissionID);
                FindProfileAndDeleteIT(emp);
                db.EmployeeModulePermission.Remove(emp);
                db.SaveChanges();
                ar.Type.Add(1);
                ar.Messages.Add(Resources.AnswerResultSuccess);
            }
            catch (DbEntityValidationException E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                foreach (var eve in E.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        ar.Type.Add(3);
                        ar.Messages.Add(ve.ErrorMessage.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                    }
                }
            }
            catch (Exception E)
            {
                ar.Type.Add(3);
                ar.Messages.Add(Resources.AnswerResultError);
                if (E.InnerException != null)
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.InnerException.InnerException.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
                else
                {
                    ar.Type.Add(3);
                    ar.Messages.Add(E.Message.Replace("'", "").Replace("\"", "").Replace("\r\n", "  "));
                }
            }
            return Json(ar);
        }
    }
}